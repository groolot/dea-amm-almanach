$pdflatex = 'pdflatex --shell-escape %O %S';
$recorder = 1;
$hash_calc_ignore_pattern{'pdf'} = '.*';
$hash_calc_ignore_pattern{'png'} = '.*';

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
  if ( $silent ) {
    system "makeglossaries -q '$_[0]'";
  }
  else {
    system "makeglossaries '$_[0]'";
  };
}
push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';
$clean_ext .= ' %R.ist %R.xdy';

add_cus_dep('dot', 'pdf', 0, 'run_dot');
sub run_dot {
  if ( $silent ) {
    system "dot -Gcharset=latin1 -Tpdf -o'$_[0].pdf' '$_[0].dot'";
  }
  else {
    system "dot -v -Gcharset=latin1 -Tpdf -o'$_[0].pdf' '$_[0].dot'";
  };
  &cus_dep_require_primary_run;
}

add_cus_dep('gpx', 'png', 0, 'run_gpx2png');
sub run_gpx2png {
  if ( $silent ) {
    system "gpx2png/gpx2png.pl -q -o '$_[0].png' '$_[0].gpx'";
  }
  else {
    system "gpx2png/gpx2png.pl -o '$_[0].png' '$_[0].gpx'";
  };
  &cus_dep_require_primary_run;
}

$success_cmd = 'gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dDownsampleColorImages=true -dColorImageResolution=300 -dNOPAUSE -dBATCH -sOutputFile=%R.small.pdf %R.pdf';